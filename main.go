package main

import (
	"sneak.berlin/x/cmd"
)

func main() {
	cmd.Execute()
}
