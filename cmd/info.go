package cmd

import (
	"fmt"
	"runtime/debug"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(infoCmd)
}

var infoCmd = &cobra.Command{
	Use:   "info",
	Short: "Print tool info",
	Run: func(cmd *cobra.Command, args []string) {
		info, _ := debug.ReadBuildInfo()
		fmt.Printf("%#v\n", info)
	},
}
