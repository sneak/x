package cmd

import (
	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(generateCommand)
}

var generateCommand = &cobra.Command{
	Use:   "generate",
	Short: "Generate things",
}
