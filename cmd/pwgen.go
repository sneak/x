package cmd

import (
	"crypto/rand"
	"fmt"
	"math/big"

	"github.com/spf13/cobra"
	"sneak.berlin/x/pkg/bip39wordlist"
)

func init() {
	generateCommand.AddCommand(passwordCmd)
}

var passwordCmd = &cobra.Command{
	Use:   "password",
	Short: "Print a randomly generated password",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("%s\n", randomPassword())
	},
}

func randomPassword() string {
	wl := bip39wordlist.WordListEnglish
	count := len(wl)
	r, err := rand.Int(rand.Reader, big.Int(count))
	return wl[r]
}
