package bip39wordlist_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"sneak.berlin/x/pkg/bip39wordlist"
)

func TestLength(t *testing.T) {
	assert := assert.New(t)

	wl := bip39wordlist.Wordlist
	assert.Equal(2048, len(wl), "should have correct number of words")
	assert.Equal(wl[0], "abandon", "first word should be correct")
	assert.Equal(wl[len(wl)-1], "zoo", "last word should be correct")
}
